import React, { useState } from 'react';
import './App.css';
import ToDoList from './components/todolist/ToDoList';
import AddToDo from './components/addToDo/AddToDo';

const ToDoListData = [
  {
    "todo": "Complete class components",
    "id": 1
  },
  {
    "todo": "learn about hooks",
    "id": 2
  }
];
// class App extends React.Component {
//   constructor(props) {
//     super(props);
//     const style = {
//       background: 'grey',
//       width: '500px',
//       height: '100vh',
//       margin: '0 auto'
//     };
//     this.attributes = {
//       style,
//       className: 'main-app',
//     };
//     this.state = {
//       todoList: ToDoListData
//     }
//   }

  
//   render() {
//     return <div {...this.attributes}>
//       <ToDoList data={this.state.todoList}/>
//       <AddToDo onAddToDo={(e) => {
//         const todoText = e.target.todoText.value;
//         if (todoText.length > 0) {
//           this.setState({
//             todoList: [
//               ...this.state.todoList,
//               {
//                 "todo": todoText,
//                 "id": this.state.todoList.length + 2
//               }
//             ]
//           })
//         }
//         e.preventDefault();
//       }}/>
//     </div>;
//   }
// }

// export default App;
const App = () => {
  const style = {
    background: 'grey',
    width: '500px',
    height: '100vh',
    margin: '0 auto'
  };
  const attributes = {
    style,
    className: 'main-app',
  };

  const [ todoList, setTodoList ] = useState(ToDoListData);  
  
  const addToDoHandler = (e) => {
    const todoText = e.target.todoText.value;
    if (todoText.length > 0) {
      setTodoList([
        ...todoList,
        {
          todo: todoText,
          id: todoList.length + 2
        }
      ]);
    }
    e.preventDefault();
  };

  const todoCompleteHandler = (id) => {
    setTodoList(todoList.map(todo => {
      if(todo.id === id)
        todo.completed = true;
      return todo;
    }));
  };

  return <div {...attributes}>
    <ToDoList data={todoList} onTodoComplete={todoCompleteHandler}/>
    <AddToDo onAddToDo={addToDoHandler}/>
  </div>;
}

export default App;