import React from 'react';

const AddToDo = (props) => {
  const submitHandler = (e) => {
    props.onAddToDo(e);
    e.target.todoText.value = '';
  };
  return <>
    <form onSubmit={submitHandler}>
      <input type="text" name="todoText"/>
      <input type="submit" value="add"/>
    </form>
  </>
};

export default AddToDo;