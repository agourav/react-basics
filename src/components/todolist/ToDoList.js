import React from 'react';

const ToDoList = (props) => {
  return <div>
    {
      props.data.map(todoItem =>
        <Todo
          key={todoItem.id}
          id={todoItem.id}
          text={todoItem.todo}
          onTodoComplete={props.onTodoComplete}
          completed={todoItem.completed}
        />)
    }
  </div>
};

export const Todo = (props) => {
  const style = {};
  if(props.completed)
    style.textDecoration = "line-through";
  return <div style={style}>
    { props.text }
    { props.completed || <button onClick={() => props.onTodoComplete(props.id)}>Done</button> }
  </div>
}

export default ToDoList;